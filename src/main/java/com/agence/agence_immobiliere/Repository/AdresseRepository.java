package com.agence.agence_immobiliere.Repository;

import com.agence.agence_immobiliere.Model.AdresseModel;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AdresseRepository extends JpaRepository<AdresseModel, Long> {
    AdresseModel findByRueAndCodePostalAndVille(String rue, Integer codePostal, String ville);
}
