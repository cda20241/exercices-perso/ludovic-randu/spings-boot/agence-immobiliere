package com.agence.agence_immobiliere.Repository;

import com.agence.agence_immobiliere.DTO.ProprietaireDTO;
import com.agence.agence_immobiliere.Enum.TypeBien;
import com.agence.agence_immobiliere.Model.BienModel;
import com.agence.agence_immobiliere.Model.PersonneModel;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface BienRepository extends JpaRepository<BienModel, Long> {

    Integer countByProprietaire(PersonneModel proprietaire);

    Integer countByAgentImmobilier(PersonneModel agent);

    List<BienModel> findByProprietaire(PersonneModel proprietaire);
}