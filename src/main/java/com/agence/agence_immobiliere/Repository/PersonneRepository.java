package com.agence.agence_immobiliere.Repository;

import com.agence.agence_immobiliere.Enum.TypePersonne;
import com.agence.agence_immobiliere.Model.PersonneModel;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface PersonneRepository extends JpaRepository<PersonneModel, Long> {
    List<PersonneModel> findByType(TypePersonne type);
    PersonneModel findByPrenomAndNom(String nom, String prenom);
}
