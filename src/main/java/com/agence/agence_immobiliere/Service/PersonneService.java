package com.agence.agence_immobiliere.Service;

import com.agence.agence_immobiliere.DTO.AgentImmobilierDTO;
import com.agence.agence_immobiliere.DTO.ProprietaireDTO;
import com.agence.agence_immobiliere.Enum.TypePersonne;
import com.agence.agence_immobiliere.Model.AdresseModel;
import com.agence.agence_immobiliere.Model.BienModel;
import com.agence.agence_immobiliere.Model.PersonneModel;
import com.agence.agence_immobiliere.Repository.AdresseRepository;
import com.agence.agence_immobiliere.Repository.BienRepository;
import com.agence.agence_immobiliere.Repository.PersonneRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class PersonneService {
    @Autowired
    private PersonneRepository personneRepository;
    @Autowired
    private AdresseRepository adresseRepository;
    @Autowired
    private AdresseService adresseService;
    @Autowired
    private BienRepository bienRepository;


// Récupération d'une liste de toutes les personnes
    public List<PersonneModel> getAllPersonnes() {
        return personneRepository.findAll();
    }
// Récupération d'une seule personne avec son ID
    public PersonneModel getPersonneById(Long id) {
        return personneRepository.findById(id).orElse(null);
    }
    public List<PersonneModel> getAllPersonnesByType(TypePersonne type) {
        return personneRepository.findByType(type);
    }
// Création d'une nouvelle personne
    public PersonneModel createPersonne(PersonneModel personne) {
        if (personne.getAdresse() != null) {    // On vérifie que l'adresse n'est pas nulle
            //AdresseModel newAdresse =  adresseService.createAdresse(personne.getAdresse());
            AdresseModel newAdresse =  adresseRepository.save(personne.getAdresse());
            personne.setAdresse(newAdresse); // Définition de l'adresse de la personne avec l'adresse enregistrée
        }else {
            personne.setAdresse(null);
        }
        // Vérification si la personne existe déjà
        PersonneModel existingPersonne = personneRepository.findByPrenomAndNom(personne.getPrenom(), personne.getNom());
        if (existingPersonne != null) {
            throw new RuntimeException("Une personne avec le même prénom et le même nom existe déjà.");
        }
        // Sauvegarde personne
        return personneRepository.save(personne);
    }
// Mise à jour d'une personne
    public PersonneModel updatePersonne(Long id, PersonneModel newPersonne) {
        Optional<PersonneModel> existingPersonneOpt = personneRepository.findById(id);
        if (existingPersonneOpt.isPresent()) {
            PersonneModel existingPersonne = existingPersonneOpt.get();
            // On verifie si chaque entrée a été modifié ou sinon on reprends l'ancienne valeur
            if (newPersonne.getPrenom() != null) {
                existingPersonne.setPrenom(newPersonne.getPrenom());
            }
            if (newPersonne.getNom() != null) {
                existingPersonne.setNom(newPersonne.getNom());
            }
            if (newPersonne.getAdresse() != null) {
                // Vérifiez si l'adresse existe déjà
                AdresseModel existingAdresse = adresseRepository.findById(newPersonne.getAdresse().getId()).orElse(null);
                if (existingAdresse == null) {
                    // Si l'adresse n'existe pas, créez une nouvelle adresse
                    AdresseModel newAdresse = new AdresseModel();
                    newAdresse.setRue(newPersonne.getAdresse().getRue());
                    newAdresse.setCodePostal(newPersonne.getAdresse().getCodePostal());
                    newAdresse.setVille(newPersonne.getAdresse().getVille());
                    adresseRepository.save(newAdresse);
                    existingPersonne.setAdresse(newAdresse);
                } else {
                    // Si l'adresse existe déjà, utilisez l'adresse existante
                    existingPersonne.setAdresse(existingAdresse);
                }
            }
            if (newPersonne.getType() != null) {
                existingPersonne.setType(newPersonne.getType());
            }

            personneRepository.save(existingPersonne);
        }else return null;
        return existingPersonneOpt.get();
    }
// Suppression d'une personne
    public void deletePersonne(Long id) {
        personneRepository.deleteById(id);
    }

// Récupération de la liste des proprietaires
    public List<ProprietaireDTO> getAllProprietaire() {
        List<PersonneModel> proprietaires = personneRepository.findByType(TypePersonne.PROPRIETAIRE);
        List<ProprietaireDTO> dtos = new ArrayList<>();
        for (PersonneModel proprietaire : proprietaires) {
            ProprietaireDTO dto = new ProprietaireDTO();
            dto.setId(proprietaire.getId());
            dto.setNom(proprietaire.getNom());
            dto.setPrenom(proprietaire.getPrenom());
            dto.setNombreDeBiens(bienRepository.countByProprietaire(proprietaire));
            dtos.add(dto);
        }
        return dtos;
    }

    public List<AgentImmobilierDTO> getAllAgentsImmobiliers() {
        List<PersonneModel> agents = personneRepository.findByType(TypePersonne.AGENT);
        List<AgentImmobilierDTO> dtos = new ArrayList<>();
        for (PersonneModel agent : agents) {
            AgentImmobilierDTO dto = new AgentImmobilierDTO();
            dto.setId(agent.getId());
            dto.setNom(agent.getNom());
            dto.setPrenom(agent.getPrenom());
            dto.setNombreDeBiens(bienRepository.countByAgentImmobilier(agent));
            dtos.add(dto);
        }
        return dtos;
    }

    /**
     * Ajouter un bien a une personne
     *
     * @param  idPersonne   L'id d'une Personne
     * @param  idBien       L'id d'un Bien a ajouter
     * @return              Le Bien ajouté
     */
    public BienModel addBienToPersonne(Long idPersonne, Long idBien) {
        PersonneModel personne = personneRepository.findById(idPersonne)
                .orElseThrow(() -> new RuntimeException("Personne non trouvée avec l'id " + idPersonne));
        BienModel bien = bienRepository.findById(idBien)
                .orElseThrow(() -> new RuntimeException("Bien non trouvé avec l'id " + idBien));
        // Association du bien à la personne
        bien.setProprietaire(personne);
        // Sauvegarde du bien avec la nouvelle personne
        bienRepository.save(bien);
        return bien;
    }

    /**
     * @param idPersonne
     * @return Liste des biens
     */
    public List<BienModel> getBiensByPersonne(Long idPersonne) {
        PersonneModel personne = personneRepository.findById(idPersonne)
                .orElseThrow(() -> new RuntimeException("Personne non trouvée avec l'id " + idPersonne));
        // Récupérer tous les biens de la personne
        return bienRepository.findByProprietaire(personne);
    }

    public BienModel removeBienFromPersonne(Long idPersonne, Long idBien) {
        PersonneModel personne = personneRepository.findById(idPersonne)
                .orElseThrow(() -> new RuntimeException("Personne non trouvée avec l'id " + idPersonne));
        BienModel bien = bienRepository.findById(idBien)
                .orElseThrow(() -> new RuntimeException("Bien non trouvé avec l'id " + idBien));

        if (!bien.getProprietaire().equals(personne)) {
            throw new RuntimeException("La personne avec l'id " + idPersonne + " ne possède pas le bien avec l'id " + idBien);
        }
        // Retirer le bien de la personne
        bien.setProprietaire(null);
        // Sauvegarder le bien sans propriétaire
        bienRepository.save(bien);
        return bien;
    }
}
