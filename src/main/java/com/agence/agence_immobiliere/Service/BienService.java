package com.agence.agence_immobiliere.Service;


import com.agence.agence_immobiliere.Model.AdresseModel;
import com.agence.agence_immobiliere.Model.BienModel;
import com.agence.agence_immobiliere.Repository.BienRepository;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

@Service
public class BienService {
    @Autowired
    private BienRepository bienRepository;


    public List<BienModel> getAllBiens() {
        return bienRepository.findAll();
    }

    public BienModel getBienById(Long id) {
        return bienRepository.findById(id).orElse(null);
    }

    public BienModel createBien(BienModel bien) { return bienRepository.save(bien); }

    public BienModel updateBien(Long id, BienModel updatedBien) {
        BienModel existingBien = bienRepository.findById(id).orElse(null);
        BeanUtils.copyProperties(updatedBien, existingBien, "id");
        return bienRepository.save(existingBien);
    }


    public void deleteBien(Long id) {
        bienRepository.deleteById(id);
    }



}
