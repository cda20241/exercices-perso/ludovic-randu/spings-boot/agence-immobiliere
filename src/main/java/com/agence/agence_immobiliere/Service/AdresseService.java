package com.agence.agence_immobiliere.Service;

import com.agence.agence_immobiliere.Model.AdresseModel;
import com.agence.agence_immobiliere.Repository.AdresseRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AdresseService {
    @Autowired
    private AdresseRepository adresseRepository;


    public List<AdresseModel> getAllAdresses() {
        return adresseRepository.findAll();
    }

    public AdresseModel createAdresse(AdresseModel adresse) {
        // Vérification si l'adresse existe déjà
        AdresseModel existingAdresse = adresseRepository.findByRueAndCodePostalAndVille(adresse.getRue(), adresse.getCodePostal(), adresse.getVille());
        if (existingAdresse != null) {
            throw new RuntimeException("Une adresse avec la meme adresse, code postal et ville existe deja.");
        }
        return adresseRepository.save(adresse);
    }

    public AdresseModel getAdresseById(Long id) {
        return adresseRepository.findById(id).orElse(null);
    }

    public AdresseModel updateAdresse(Long id, AdresseModel newAdresse) {
        AdresseModel existingAdresse = adresseRepository.findById(id)
                .orElseThrow(() -> new RuntimeException("Adresse non trouvée"));

        // Mise à jour des champs de l'adresse existante
        if (newAdresse.getRue() != null) {
            existingAdresse.setRue(newAdresse.getRue());
        }
        if (newAdresse.getCodePostal() != null) {
            existingAdresse.setCodePostal(newAdresse.getCodePostal());
        }
        if (newAdresse.getVille() != null) {
            existingAdresse.setVille(newAdresse.getVille());
        }

        // Enregistrement de l'adresse mise à jour
        return adresseRepository.save(existingAdresse);

    }

    public void deleteAdresse(Long id) {
        adresseRepository.deleteById(id);
    }



}
