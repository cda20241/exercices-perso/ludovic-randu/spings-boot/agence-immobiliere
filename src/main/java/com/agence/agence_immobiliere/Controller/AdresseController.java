package com.agence.agence_immobiliere.Controller;

import com.agence.agence_immobiliere.Model.AdresseModel;
import com.agence.agence_immobiliere.Repository.AdresseRepository;
import com.agence.agence_immobiliere.Service.AdresseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/adresse")
public class AdresseController {

    @Autowired
    private final AdresseRepository adresseRepository;
    @Autowired
    private final AdresseService adresseService;

    public AdresseController(AdresseRepository adresseRepository, AdresseService adresseService) {
        this.adresseRepository = adresseRepository;
        this.adresseService = adresseService;
    }

    // Méthodes CRUD
    @PostMapping
    public AdresseModel createAdresse(@RequestBody AdresseModel adresse) {
        return adresseService.createAdresse(adresse);
    }

    // GET
    @GetMapping("/all")
    public List<AdresseModel> getAllAdresses() {
        return adresseService.getAllAdresses();
    }
    @GetMapping("/{id}")
    public AdresseModel getAdresseById(Long id) {
        return adresseService.getAdresseById(id);
    }

    // MàJ
    @PutMapping("/{id}")
    public AdresseModel updateAdresse(@PathVariable Long id, @RequestBody AdresseModel newAdresse) {
        return adresseService.updateAdresse(id, newAdresse);
    }


    // Suppression
    @DeleteMapping("/{id}")
    public void deleteAdresse(@PathVariable Long id) {
        adresseService.deleteAdresse(id);
    }

}
