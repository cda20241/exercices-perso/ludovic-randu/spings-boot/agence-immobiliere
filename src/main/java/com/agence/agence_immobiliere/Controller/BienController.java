package com.agence.agence_immobiliere.Controller;

import com.agence.agence_immobiliere.Model.BienModel;
import com.agence.agence_immobiliere.Repository.BienRepository;
import com.agence.agence_immobiliere.Service.BienService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/bien")
public class BienController {
    @Autowired
    private BienRepository bienRepository;
    @Autowired
    private final BienService bienService;

    public BienController(BienRepository bienRepository, BienService bienService) {
        this.bienRepository = bienRepository;
        this.bienService = bienService;
    }

    // méthodes CRUD
    @PostMapping
    public BienModel createBien(@RequestBody BienModel bien) {
        return bienService.createBien(bien);
    }

// Lecture
    @GetMapping("/{id}")
    public BienModel getBien(@PathVariable Long id) {
        return bienService.getBienById(id);
    }

    @GetMapping("/all")
    public List<BienModel> getAllBiens() {
        return bienService.getAllBiens();
    }

// Mise a jour
    @PutMapping("/{id}")
    public BienModel updateAdresse(@PathVariable Long id, @RequestBody BienModel newBien) {
        return bienService.updateBien(id, newBien);
    }
// Suppression
    @DeleteMapping("/{id}")
    public void deleteBien(@PathVariable Long id) {
        bienService.deleteBien(id);
    }
}
