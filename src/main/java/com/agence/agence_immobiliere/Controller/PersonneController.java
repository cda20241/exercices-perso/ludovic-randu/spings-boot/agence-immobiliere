package com.agence.agence_immobiliere.Controller;

import com.agence.agence_immobiliere.DTO.AgentImmobilierDTO;
import com.agence.agence_immobiliere.DTO.ProprietaireDTO;
import com.agence.agence_immobiliere.Enum.TypePersonne;
import com.agence.agence_immobiliere.Model.BienModel;
import com.agence.agence_immobiliere.Model.PersonneModel;
import com.agence.agence_immobiliere.Repository.AdresseRepository;
import com.agence.agence_immobiliere.Repository.PersonneRepository;
import com.agence.agence_immobiliere.Service.PersonneService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/personne")
public class PersonneController {
    @Autowired
    private PersonneRepository personneRepository;
    @Autowired
    private AdresseRepository adresseRepository;
    @Autowired
    private PersonneService personneService;

    // CONSTRUCTEUR
    public PersonneController(PersonneRepository personneRepository,
                              AdresseRepository adresseRepository,
                              PersonneService personneService) {
        // C'est Spring qui se charge d'instancier la classe SalarieController, et en le faisant il voit
        // qu'il doit aussi créer une instance de SalarieRepository et nous la passer ici
        this.personneRepository = personneRepository;
        this.adresseRepository = adresseRepository;
        this.personneService = personneService;
    }



    @GetMapping("/all")
    public List<PersonneModel> getAllPersonnes() {
        return personneService.getAllPersonnes();
    }
    @GetMapping("/proprietaires")
    public List<ProprietaireDTO> getAllProprietaireVendeurs() {
        return personneService.getAllProprietaire();
    }
    @GetMapping("/agents")
    public List<AgentImmobilierDTO> getAllAgentsImmobiliers() {
        return personneService.getAllAgentsImmobiliers();
    }
    @GetMapping("/")
    public List<PersonneModel> getAllPersonnes(@RequestParam(required = false) TypePersonne type) {
        if (type != null) {
            return personneService.getAllPersonnesByType(type);
        } else {
            return personneService.getAllPersonnes();
        }
    }

    @GetMapping("/{id}")
    public PersonneModel getPersonneById(@PathVariable Long id) {
        return personneService.getPersonneById(id);
    }
    @GetMapping("/{id}/biens")
    public List<BienModel> getBiensByPersonne(@PathVariable Long id) {
        return personneService.getBiensByPersonne(id);
    }

    @PostMapping("/{idPersonne}/biens/{idBien}")
    public BienModel addBienToPersonne(@PathVariable Long idPersonne, @PathVariable Long idBien) {
        return personneService.addBienToPersonne(idPersonne, idBien);
    }

    @PostMapping("")
    public PersonneModel createPersonne(@RequestBody PersonneModel newPersonne) {
        return personneService.createPersonne(newPersonne);
    }

    @PutMapping("/{id}")
    public PersonneModel updatePersonne(@PathVariable Long id, @RequestBody PersonneModel newPersonne) {
        return personneService.updatePersonne(id, newPersonne);
    }

    @DeleteMapping("/{id}")
    public void deletePersonne(@PathVariable Long id) {
         personneService.deletePersonne(id);
    }

}
