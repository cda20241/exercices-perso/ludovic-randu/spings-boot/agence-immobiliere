package com.agence.agence_immobiliere.DTO;

public class AgentImmobilierDTO {
    private Long id;
    private String nom;
    private String prenom;
    private Integer nombreDeBiens;

    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }
    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }
    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public Integer getNombreDeBiens() {
        return nombreDeBiens;
    }
    public void setNombreDeBiens(Integer nombreDeBiens) {
        this.nombreDeBiens = nombreDeBiens;
    }
}
