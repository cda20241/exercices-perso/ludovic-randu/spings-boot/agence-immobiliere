package com.agence.agence_immobiliere.Enum;

public enum TypeBien {
    MAISON,
    IMMEUBLE,
    APPARTEMENT
}
