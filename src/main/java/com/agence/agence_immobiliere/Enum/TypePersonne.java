package com.agence.agence_immobiliere.Enum;

public enum TypePersonne {
    AGENT,
    PROSPECT,
    LOCATAIRE,
    PROPRIETAIRE,
}