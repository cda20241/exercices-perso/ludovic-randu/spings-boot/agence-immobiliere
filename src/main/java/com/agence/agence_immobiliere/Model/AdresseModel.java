package com.agence.agence_immobiliere.Model;

import jakarta.persistence.*;

@Entity
//@Table(uniqueConstraints = @UniqueConstraint(columnNames = {"rue", "codePostal", "ville"}))
public class AdresseModel {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String rue;
    private Integer codePostal;
    private String ville;


    // Constructeurs
    public AdresseModel() {

    }
    public AdresseModel(String rue, Integer codePostal, String ville) {
        this.rue = rue;
        this.codePostal = codePostal;
        this.ville = ville;
    }

    // getters et setters
    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }

    public String getRue() {
        return rue;
    }
    public void setRue(String rue) {
        this.rue = rue;
    }

    public Integer getCodePostal() {
        return codePostal;
    }
    public void setCodePostal(Integer codePostal) {
        this.codePostal = codePostal;
    }

    public String getVille() {
        return ville;
    }
    public void setVille(String ville) {
        this.ville = ville;
    }
}