package com.agence.agence_immobiliere.Model;

import com.agence.agence_immobiliere.Enum.TypeBien;
import jakarta.persistence.*;

@Entity
public class BienModel {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @ManyToOne
    private AdresseModel adresse;
    @Enumerated(EnumType.STRING)
    private TypeBien type;
    @ManyToOne
    private PersonneModel locataire;
    @ManyToOne
    private PersonneModel proprietaire;
    @ManyToOne
    private PersonneModel agentImmobilier;
    @ManyToOne
    private PersonneModel prospect;

    public BienModel() {
    }

    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }

    public AdresseModel getAdresse() {
        return adresse;
    }
    public void setAdresse(AdresseModel adresse) {
        this.adresse = adresse;
    }

    public TypeBien getType() {
        return type;
    }
    public void setType(TypeBien type) {
        this.type = type;
    }

    public PersonneModel getLocataire() {
        return locataire;
    }
    public void setLocataire(PersonneModel locataire) {
        this.locataire = locataire;
    }

    public PersonneModel getProprietaire() {
        return proprietaire;
    }
    public void setProprietaire(PersonneModel proprietaireVendeur) {
        this.proprietaire = proprietaireVendeur;
    }

    public PersonneModel getAgentImmobilier() {
        return agentImmobilier;
    }
    public void setAgentImmobilier(PersonneModel agentImmobilier) {
        this.agentImmobilier = agentImmobilier;
    }

    public PersonneModel getProspect() {
        return prospect;
    }
    public void setProspect(PersonneModel prospect) {
        this.prospect = prospect;
    }
}