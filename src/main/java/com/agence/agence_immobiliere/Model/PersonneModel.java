package com.agence.agence_immobiliere.Model;

import com.agence.agence_immobiliere.Enum.TypePersonne;
import jakarta.persistence.*;

// Modèle pour Personne
@Entity
public class PersonneModel {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String nom;
    private String prenom;
    @ManyToOne
    private AdresseModel adresse;
    @Enumerated(EnumType.STRING)
    private TypePersonne type;

    public PersonneModel() {
    }

    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }
    public void setNom(String nom) {
        this.nom = nom;
    }

    public AdresseModel getAdresse() {
        return adresse;
    }
    public void setAdresse(AdresseModel adresse) {
        this.adresse = adresse;
    }

    public String getPrenom() {
        return prenom;
    }
    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public TypePersonne getType() {
        return type;
    }
    public void setType(TypePersonne type) {
        this.type = type;
    }
    // getters et setters

}