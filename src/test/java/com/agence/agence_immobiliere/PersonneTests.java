package com.agence.agence_immobiliere;

import com.agence.agence_immobiliere.DTO.AgentImmobilierDTO;
import com.agence.agence_immobiliere.DTO.ProprietaireDTO;
import com.agence.agence_immobiliere.Enum.TypePersonne;
import com.agence.agence_immobiliere.Model.AdresseModel;
import com.agence.agence_immobiliere.Model.BienModel;
import com.agence.agence_immobiliere.Model.PersonneModel;
import com.agence.agence_immobiliere.Repository.AdresseRepository;
import com.agence.agence_immobiliere.Repository.BienRepository;
import com.agence.agence_immobiliere.Repository.PersonneRepository;
import com.agence.agence_immobiliere.Service.PersonneService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.dao.EmptyResultDataAccessException;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RunWith(MockitoJUnitRunner.class)
public class PersonneTests {

    @Mock
    private PersonneRepository personneRepository;
    @Mock
    private BienRepository bienRepository;
    @Mock
    private AdresseRepository adresseRepository;
    @InjectMocks
    private PersonneService personneService;

    @Test
    public void testGetAllPersonnes() {
        // Préparation des données
        List<PersonneModel> personnes = new ArrayList<>();
        personnes.add(new PersonneModel());
        // Configuration des Mocks
        Mockito.when(personneRepository.findAll()).thenReturn(personnes);
        // Appel de la méthode à tester
        List<PersonneModel> result = personneService.getAllPersonnes();
        // Vérification du résultat
        Assert.assertEquals(personnes, result);
    }
    @Test
    public void testGetPersonneById() {
        // Préparation des données
        PersonneModel personne = new PersonneModel();
        personne.setId(1L);
        // Configuration des Mocks
        Mockito.when(personneRepository.findById(1L)).thenReturn(Optional.of(personne));
        // Appel de la méthode à tester
        PersonneModel result = personneService.getPersonneById(1L);
        // Vérification du résultat
        Assert.assertEquals(personne, result);
    }
    @Test
    public void testGetAllPersonnesByType() {
        // Préparation des données
        List<PersonneModel> personnes = new ArrayList<>();
        personnes.add(new PersonneModel());
        // Configuration des Mocks
        Mockito.when(personneRepository.findByType(TypePersonne.PROPRIETAIRE)).thenReturn(personnes);
        // Appel de la méthode à tester
        List<PersonneModel> result = personneService.getAllPersonnesByType(TypePersonne.PROPRIETAIRE);
        // Vérification du résultat
        Assert.assertEquals(personnes, result);
    }
    @Test
    public void testCreatePersonne() {
        // Préparer les données
        PersonneModel personne = new PersonneModel();
        personne.setPrenom("Jean");
        personne.setNom("Dupont");
        AdresseModel adresse = new AdresseModel();
        adresse.setRue("Rue de la Paix");
        personne.setAdresse(adresse);
        // Configurer les maquettes
        Mockito.when(adresseRepository.save(adresse)).thenReturn(adresse);
        Mockito.when(personneRepository.findByPrenomAndNom("Jean", "Dupont")).thenReturn(null);
        Mockito.when(personneRepository.save(personne)).thenReturn(personne);
        // Appeler la méthode à tester
        PersonneModel result = personneService.createPersonne(personne);
        // Vérifier le résultat
        Assert.assertEquals(personne, result);
    }
    @Test(expected = RuntimeException.class)
    public void testCreatePersonne_ExistingPersonne() {
        // Préparer les données
        PersonneModel personne = new PersonneModel();
        personne.setPrenom("Jean");
        personne.setNom("Dupont");
        // Configurer les maquettes
        Mockito.when(personneRepository.findByPrenomAndNom("Jean", "Dupont")).thenReturn(personne);
        // Appeler la méthode à tester
        personneService.createPersonne(personne);
    }

    @Test
    public void testUpdatePersonne() {
        // Préparer les données
        PersonneModel existingPersonne = new PersonneModel();
        existingPersonne.setId(1L);
        existingPersonne.setPrenom("Jean");
        existingPersonne.setNom("Dupont");
        AdresseModel existingAdresse = new AdresseModel();
        existingAdresse.setId(1L);
        existingAdresse.setRue("Rue de la Paix");
        existingPersonne.setAdresse(existingAdresse);

        PersonneModel newPersonne = new PersonneModel();
        newPersonne.setPrenom("Jean");
        newPersonne.setNom("Doe");
        AdresseModel newAdresse = new AdresseModel();
        newAdresse.setId(1L);
        newAdresse.setRue("Rue du Commerce");
        newPersonne.setAdresse(newAdresse);
        // Configurer les maquettes
        Mockito.when(personneRepository.findById(1L)).thenReturn(Optional.of(existingPersonne));
        Mockito.when(adresseRepository.findById(1L)).thenReturn(Optional.of(existingAdresse));
        Mockito.when(personneRepository.save(existingPersonne)).thenReturn(existingPersonne);
        // Appeler la méthode à tester
        PersonneModel result = personneService.updatePersonne(1L, newPersonne);
        // Vérifier le résultat
        Assert.assertEquals("Jean", result.getPrenom());
        Assert.assertEquals("Doe", result.getNom());
        Assert.assertEquals("Rue de la Paix", result.getAdresse().getRue()); // L'adresse ne devrait pas être mise à jour
    }
    @Test
    public void testUpdatePersonne_NonExistingPersonne() {
        // Préparer les données
        PersonneModel newPersonne = new PersonneModel();
        newPersonne.setPrenom("Jean");
        newPersonne.setNom("Doe");
        // Configurer les maquettes
        Mockito.when(personneRepository.findById(1L)).thenReturn(Optional.empty());
        // Appeler la méthode à tester
        PersonneModel result = personneService.updatePersonne(1L, newPersonne);
        // Vérifier le résultat
        Assert.assertNull(result);
    }



    @Test
    public void testDeletePersonne() {
        // Préparation des données
        Long id = 1L;
        // Configuration des Mocks
        Mockito.doNothing().when(personneRepository).deleteById(id);
        // Appeler la méthode à tester
        personneService.deletePersonne(id);
        // Vérification que la méthode deleteById a été appelée
        Mockito.verify(personneRepository, Mockito.times(1)).deleteById(id);
    }

    @Test(expected = EmptyResultDataAccessException.class)
    public void testDeletePersonne_NonExistingPersonne() {
        // Préparation des données
        Long id = 1L;
        // Configuration des Mocks
        Mockito.doThrow(new EmptyResultDataAccessException(1)).when(personneRepository).deleteById(id);
        // Appel de la méthode à tester
        personneService.deletePersonne(id);
    }

    @Test
    public void testGetAllProprietaire() {
        // Préparer les données
        List<PersonneModel> proprietaires = new ArrayList<>();
        PersonneModel proprietaire = new PersonneModel();
        proprietaire.setId(1L);
        proprietaire.setNom("Dupont");
        proprietaire.setPrenom("Jean");
        proprietaires.add(proprietaire);
        // Configurer les maquettes
        Mockito.when(personneRepository.findByType(TypePersonne.PROPRIETAIRE)).thenReturn(proprietaires);
        Mockito.when(bienRepository.countByProprietaire(proprietaire)).thenReturn(10); // Convertir en Long
        // Appel de la méthode à tester
        List<ProprietaireDTO> result = personneService.getAllProprietaire();
        // Vérifier le résultat
        Assert.assertEquals(1, result.size());
        ProprietaireDTO dto = result.get(0);
        Assert.assertEquals("Dupont", dto.getNom());
        Assert.assertEquals("Jean", dto.getPrenom());
        Assert.assertEquals(Integer.valueOf(10), dto.getNombreDeBiens());
    }

    @Test
    public void testGetAllAgentsImmobiliers() {
        // Préparer les données
        List<PersonneModel> agents = new ArrayList<>();
        PersonneModel agent = new PersonneModel();
        agent.setId(1L);
        agent.setNom("Dupont");
        agent.setPrenom("Jean");
        agents.add(agent);
        // Configurer les maquettes
        Mockito.when(personneRepository.findByType(TypePersonne.AGENT)).thenReturn(agents);
        Mockito.when(bienRepository.countByAgentImmobilier(agent)).thenReturn(10);
        // Appeler la méthode à tester
        List<AgentImmobilierDTO> result = personneService.getAllAgentsImmobiliers();
        // Vérifier le résultat
        Assert.assertEquals(1, result.size());
        AgentImmobilierDTO dto = result.get(0);
        Assert.assertEquals("Dupont", dto.getNom());
        Assert.assertEquals("Jean", dto.getPrenom());
        Assert.assertEquals(Integer.valueOf(10), dto.getNombreDeBiens());
    }

    @Test
    public void testAddBienToPersonne() {
        // Préparation des données
        PersonneModel personne = new PersonneModel();
        personne.setId(1L);
        BienModel bien = new BienModel();
        bien.setId(1L);
        // Configuration des Mocks
        Mockito.when(personneRepository.findById(1L)).thenReturn(Optional.of(personne));
        Mockito.when(bienRepository.findById(1L)).thenReturn(Optional.of(bien));
        Mockito.when(bienRepository.save(bien)).thenReturn(bien);
        // Appel de la méthode à tester
        BienModel result = personneService.addBienToPersonne(1L, 1L);
        // Vérification du résultat
        Assert.assertEquals(bien, result);
        Assert.assertEquals(personne, result.getProprietaire());
    }
    @Test
    public void testGetBiensByPersonne() {
        // Préparer les données
        PersonneModel personne = new PersonneModel();
        personne.setId(1L);
        List<BienModel> biens = new ArrayList<>();
        biens.add(new BienModel());
        // Configurer les maquettes
        Mockito.when(personneRepository.findById(1L)).thenReturn(Optional.of(personne));
        Mockito.when(bienRepository.findByProprietaire(personne)).thenReturn(biens);
        // Appeler la méthode à tester
        List<BienModel> result = personneService.getBiensByPersonne(1L);
        // Vérifier le résultat
        Assert.assertEquals(biens, result);
    }

    @Test
    public void testRemoveBienFromPersonne() {
        // Préparation des données
        PersonneModel personne = new PersonneModel();
        personne.setId(1L);
        BienModel bien = new BienModel();
        bien.setId(1L);
        bien.setProprietaire(personne);
        // Configuration des Mocks
        Mockito.when(personneRepository.findById(1L)).thenReturn(Optional.of(personne));
        Mockito.when(bienRepository.findById(1L)).thenReturn(Optional.of(bien));
        Mockito.when(bienRepository.save(bien)).thenReturn(bien);
        // Appel de la méthode à tester
        BienModel result = personneService.removeBienFromPersonne(1L, 1L);
        // Vérification du résultat
        Assert.assertEquals(bien, result);
        Assert.assertNull(result.getProprietaire());
    }


    @Test
    public void testUpdatePersonne_CreateNewAdresse() {
        // Préparer les données
        PersonneModel existingPersonne = new PersonneModel();
        existingPersonne.setId(1L);
        existingPersonne.setPrenom("Jean");
        existingPersonne.setNom("Dupont");
        AdresseModel existingAdresse = new AdresseModel();
        existingAdresse.setId(1L);
        existingAdresse.setRue("Rue de la Paix");
        existingPersonne.setAdresse(existingAdresse);

        PersonneModel newPersonne = new PersonneModel();
        newPersonne.setPrenom("Jean");
        newPersonne.setNom("Doe");
        AdresseModel newAdresse = new AdresseModel();
        newAdresse.setId(2L);
        newAdresse.setRue("Rue du Commerce");
        newPersonne.setAdresse(newAdresse);

        // Configurer les maquettes
        Mockito.when(personneRepository.findById(1L)).thenReturn(Optional.of(existingPersonne));
        Mockito.when(adresseRepository.findById(2L)).thenReturn(Optional.empty());
        Mockito.when(adresseRepository.save(newAdresse)).thenReturn(newAdresse);
        Mockito.when(personneRepository.save(existingPersonne)).thenReturn(existingPersonne);

        // Appeler la méthode à tester
        PersonneModel result = personneService.updatePersonne(1L, newPersonne);

        // Vérifier le résultat
        Assert.assertEquals("Jean", result.getPrenom());
        Assert.assertEquals("Doe", result.getNom());
        Assert.assertEquals("Rue du Commerce", result.getAdresse().getRue());
    }

    /**
     * vérifie que lorsqu’une nouvelle adresse est fournie avec la nouvelle personne,
     * et que cette adresse n’existe pas déjà, une nouvelle adresse est créée et associée à la personne existante.
     */
    @Test
    public void testUpdatePersonne_NonExistingAdresse() {
        // Préparer les données
        PersonneModel existingPersonne = new PersonneModel();
        existingPersonne.setId(1L);
        existingPersonne.setPrenom("Jean");
        existingPersonne.setNom("Dupont");
        AdresseModel existingAdresse = new AdresseModel();
        existingAdresse.setId(1L);
        existingAdresse.setRue("Rue de la Paix");
        existingPersonne.setAdresse(existingAdresse);

        PersonneModel newPersonne = new PersonneModel();
        newPersonne.setPrenom("Jean");
        newPersonne.setNom("Doe");
        AdresseModel newAdresse = new AdresseModel();
        newAdresse.setId(2L);
        newAdresse.setRue("Rue du Commerce");
        newPersonne.setAdresse(newAdresse);

        // Configurer les maquettes
        Mockito.when(personneRepository.findById(1L)).thenReturn(Optional.of(existingPersonne));
        Mockito.when(adresseRepository.findById(2L)).thenReturn(Optional.empty());
        Mockito.when(adresseRepository.save(newAdresse)).thenReturn(newAdresse);
        Mockito.when(personneRepository.save(existingPersonne)).thenReturn(existingPersonne);

        // Appeler la méthode à tester
        PersonneModel result = personneService.updatePersonne(1L, newPersonne);

        // Vérifier le résultat
        Assert.assertEquals("Jean", result.getPrenom());
        Assert.assertEquals("Doe", result.getNom());
        Assert.assertEquals("Rue du Commerce", result.getAdresse().getRue());
    }
    /**
     * Vérification de la méthode getAllProprietaires() lorsque aucun proprietaire ne soit présent dans la BDD.
     */
    @Test
    public void testGetAllProprietaire_NoProprietaires() {
        // Configuration des Mocks
        Mockito.when(personneRepository.findByType(TypePersonne.PROPRIETAIRE)).thenReturn(new ArrayList<>());
        // Appel de la méthode à tester
        List<ProprietaireDTO> result = personneService.getAllProprietaire();
        // Vérification du résultat
        Assert.assertTrue(result.isEmpty());
    }
    /**
     * Vérification de la méthode getAllAgentsImmobiliers() lorsque aucun agent immobilier ne soit présent dans la BDD.
     */
    @Test
    public void testGetAllAgentsImmobiliers_NoAgents() {
        // Configuration des Mocks
        Mockito.when(personneRepository.findByType(TypePersonne.AGENT)).thenReturn(new ArrayList<>());
        // Appel de la méthode à tester
        List<AgentImmobilierDTO> result = personneService.getAllAgentsImmobiliers();
        // Vérification du résultat
        Assert.assertTrue(result.isEmpty());
    }
}