package com.agence.agence_immobiliere;
import com.agence.agence_immobiliere.Model.BienModel;
import com.agence.agence_immobiliere.Repository.BienRepository;
import com.agence.agence_immobiliere.Service.BienService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Optional;

import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class BienTests {

    @InjectMocks
    private BienService bienService;

    @Mock
    private BienRepository bienRepository;

    @Test
    public void testGetAllBiens() {
        bienService.getAllBiens();
        verify(bienRepository, times(1)).findAll();
    }

    @Test
    public void testGetBienById() {
        Long id = 1L;
        bienService.getBienById(id);
        verify(bienRepository, times(1)).findById(id);
    }

    @Test
    public void testCreateBien() {
        BienModel bien = new BienModel();
        bienService.createBien(bien);
        verify(bienRepository, times(1)).save(bien);
    }

    @Test
    public void testUpdateBien() {
        Long id = 1L;
        BienModel updatedBien = new BienModel();
        when(bienRepository.findById(id)).thenReturn(Optional.of(new BienModel()));
        bienService.updateBien(id, updatedBien);
        verify(bienRepository, times(1)).save(any(BienModel.class));
    }

    @Test
    public void testDeleteBien() {
        Long id = 1L;
        bienService.deleteBien(id);
        verify(bienRepository, times(1)).deleteById(id);
    }
}