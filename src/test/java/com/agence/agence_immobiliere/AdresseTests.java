package com.agence.agence_immobiliere;

import com.agence.agence_immobiliere.Model.AdresseModel;
import com.agence.agence_immobiliere.Repository.AdresseRepository;
import com.agence.agence_immobiliere.Service.AdresseService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import java.util.Optional;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class AdresseTests {
    @InjectMocks
    private AdresseService adresseService;
    @Mock
    private AdresseRepository adresseRepository;

    /**
     * Test de la fonction getAllAdresses
     */
    @Test
    public void testGetAllAdresses() {
        adresseService.getAllAdresses();
        verify(adresseRepository, times(1)).findAll();
    }
    /**
     * Test de la fonction getAdresseById
     */
    @Test
    public void testGetAdresseById() {
        Long id = 1L;
        adresseService.getAdresseById(id);
        verify(adresseRepository, times(1)).findById(id);
    }
    /**
     * Test de la fonction createAdresse
     */
    @Test
    public void testCreateAdresse()  throws Exception {
        AdresseModel adresse = new AdresseModel();
        adresseService.createAdresse(adresse);
        verify(adresseRepository, times(1)).save(adresse);
    }
    /**
     * Test de la fonction updateAdresse
     */
    @Test
    public void testUpdateAdresse() {
        Long id = 1L;
        AdresseModel updatedAdresse = new AdresseModel();
        when(adresseRepository.findById(id)).thenReturn(Optional.of(new AdresseModel()));
        adresseService.updateAdresse(id, updatedAdresse);
        verify(adresseRepository, times(1)).save(any(AdresseModel.class));
    }
    /**
     * Test de la fonction deleteAdresse
     */
    @Test
    public void testDeleteAdresse() {
        Long id = 1L;
        adresseService.deleteAdresse(id);
        verify(adresseRepository, times(1)).deleteById(id);
    }
}
